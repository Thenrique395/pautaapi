package com.desafio.Pauta.PautaApi.ClientAPI;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "cpfClient", url = "https://user-info.herokuapp.com")
public interface ConsultaCpfClient {

    @RequestMapping(method = RequestMethod.GET, value = "/users/{cpf}", consumes = "application/json")
    String validarCpf(@PathVariable("cpf") String cpf);
}
