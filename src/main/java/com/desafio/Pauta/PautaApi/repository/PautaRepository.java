package com.desafio.Pauta.PautaApi.repository;


import com.desafio.Pauta.PautaApi.domain.Pauta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PautaRepository extends JpaRepository<Pauta, Long> {
}
