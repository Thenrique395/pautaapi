package com.desafio.Pauta.PautaApi.repository;

import com.desafio.Pauta.PautaApi.domain.Voto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VotoRepository extends JpaRepository<Voto, Long> {

    @Query(value = "Select sum(case when voto = 'SIM' then 1 else 0 end) as SIM,\n" +
            "\t   sum(case when voto = 'SIM' then 0 else 1 end) as NAO \n" +
            "\t   from tbn_voto where fk_voto_pauta= (:id);", nativeQuery = true)
    List<Object[]> resultadoVotacao(@Param("id") Long id);


    @Query(value = "select id_voto, data_registro, voto, fk_voto_pauta, fk_voto_Associado\n" +
            "    from tbn_voto where fk_voto_Associado = (:idPauta) and fk_voto_pauta =(:idAssociado)", nativeQuery = true)
    Voto validarVotoExistente(@Param("idPauta") Long idPauta, @Param("idAssociado") Long idAssociado);

}
