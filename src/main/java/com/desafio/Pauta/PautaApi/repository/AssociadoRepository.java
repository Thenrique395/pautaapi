package com.desafio.Pauta.PautaApi.repository;

import com.desafio.Pauta.PautaApi.domain.Associado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AssociadoRepository extends JpaRepository<Associado, Long> {

    @Query(value = "SELECT * FROM public.tbn_associado where cpf like (:Cpf);", nativeQuery = true)
    Associado findAllById(@Param("Cpf") String Cpf);

    @Query(value = "delete from tbn_associado where id_associado= (:id);", nativeQuery = true)
    Associado removerAssociado(@Param("id") long id);


}


