package com.desafio.Pauta.PautaApi.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode()
public class VotoAssociadoDto {

    private String voto;
    private String cpfAssociado;
    private Long pauta;


    public String getVoto() {
        return voto;
    }

    public void setVoto(String voto) {
        this.voto = voto;
    }

    public String getCpfAssociado() {
        return cpfAssociado;
    }

    public void setCpfAssociado(String cpfAssociado) {
        this.cpfAssociado = cpfAssociado;
    }

    public Long getPauta() {
        return pauta;
    }


    public void setPauta(Long pauta) {
        this.pauta = pauta;
    }
}
