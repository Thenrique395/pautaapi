package com.desafio.Pauta.PautaApi.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode()
public class PautaDto {

    private Long idpauta;
    private String tituloPauta;
    private String Autor;
    private String DetalhePauta;

    public Long getIdpauta() {
        return idpauta;
    }

    public void setIdpauta(Long idpauta) {
        this.idpauta = idpauta;
    }

    public String getTituloPauta() {
        return tituloPauta;
    }

    public void setTituloPauta(String tituloPauta) {
        this.tituloPauta = tituloPauta;
    }

    public String getAutor() {
        return Autor;
    }

    public void setAutor(String autor) {
        Autor = autor;
    }

    public String getDetalhePauta() {
        return DetalhePauta;
    }

    public void setDetalhePauta(String detalhePauta) {
        DetalhePauta = detalhePauta;
    }
}
