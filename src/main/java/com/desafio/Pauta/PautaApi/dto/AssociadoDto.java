package com.desafio.Pauta.PautaApi.dto;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode()
public class AssociadoDto {

    private Long idAssociado;
    private String nome;
    private String cpf;
    private String email;
    private Long fk_pauta_Associado;

    public Long getIdAssociado() {
        return idAssociado;
    }

    public void setIdAssociado(Long idAssociado) {
        this.idAssociado = idAssociado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getFk_pauta_Associado() {
        return fk_pauta_Associado;
    }

    public void setFk_pauta_Associado(Long fk_pauta_Associado) {
        this.fk_pauta_Associado = fk_pauta_Associado;
    }
}
