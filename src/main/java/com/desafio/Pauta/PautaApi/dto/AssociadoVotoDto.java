package com.desafio.Pauta.PautaApi.dto;


import com.desafio.Pauta.PautaApi.domain.Voto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode()
public class AssociadoVotoDto {

    private Long id;
    private String nome;
    private String cpf;
    private String email;
    private List<Voto> votos = new ArrayList<Voto>();

}
