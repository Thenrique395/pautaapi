package com.desafio.Pauta.PautaApi.dto;

public class PautaRetornoDto {

    private Long idpauta;
    private String tituloPauta;
    private String autor;
    private String detalhePauta;
    private String status;
    private String dataCriacaoPauta;

    public Long getIdpauta() {
        return idpauta;
    }

    public void setIdpauta(Long idpauta) {
        this.idpauta = idpauta;
    }

    public String getTituloPauta() {
        return tituloPauta;
    }

    public void setTituloPauta(String tituloPauta) {
        this.tituloPauta = tituloPauta;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getDetalhePauta() {
        return detalhePauta;
    }

    public void setDetalhePauta(String detalhePauta) {
        this.detalhePauta = detalhePauta;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDataCriacaoPauta() {
        return dataCriacaoPauta;
    }

    public void setDataCriacaoPauta(String dataCriacaoPauta) {
        this.dataCriacaoPauta = dataCriacaoPauta;
    }

}
