package com.desafio.Pauta.PautaApi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "tbn_associado")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode()
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Associado {

    @Id
    @ApiModelProperty(value = "Id da entidade Associado")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "idAssociado", nullable = false)
    private Long idAssociado;

    @NotBlank(message = "Nome do Associado não pode ser em branco.")
    @NotNull(message = "Nome do Associado não pode ser em nulo.")
    @ApiModelProperty(value = "Nome do Associado")
    @Size(min = 3, max = 1000)
    @Column(name = "nome", nullable = false)
    private String nome;

    @NotBlank
    @ApiModelProperty(value = "cpf do Associado")
    @Size(min = 11, max = 11, message = "Cpf contém 11 caracteres")
    @Column(name = "cpf", nullable = false)
    private String cpf;

    @Email(message = "Verificar se realmente é um e-mail.")
    @NotBlank(message = "E-mail do Associado não pode ser em branco.")
    @NotNull(message = "E-mail do Associado não pode ser em nulo.")
    @ApiModelProperty(value = "E-mail do Associado")
    @Size(min = 10, max = 1000)
    @Column(name = "email", nullable = false)
    private String email;

    @ApiModelProperty(value = "Lista de votos")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Voto.class)
    @JoinColumn(name = "fk_voto_Associado")
    private List<Voto> voto;

    @ApiModelProperty(value = "Lista de votos")
    @ManyToOne(targetEntity = Pauta.class)
    @JoinColumn(name = "fk_pauta_Associado")
    private Pauta pauta;

    public Long getIdAssociado() {
        return idAssociado;
    }

    public void setIdAssociado(Long idAssociado) {
        this.idAssociado = idAssociado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Voto> getVoto() {
        return voto;
    }

    public void setVoto(List<Voto> voto) {
        this.voto = voto;
    }

    public Pauta getPauta() {
        return pauta;
    }

    public void setPauta(Pauta pauta) {
        this.pauta = pauta;
    }
}
