package com.desafio.Pauta.PautaApi.domain.constantes;

public interface Status {
    String CRIADO = "CRIADO";
    String ENCERRADO = "ENCERRADO";
    String ATIVO = "ATIVO";
}
