package com.desafio.Pauta.PautaApi.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tbn_pauta")
@Getter
@Setter
@NoArgsConstructor()
@EqualsAndHashCode()
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Pauta {

    @Id
    @ApiModelProperty(value = "Id da pauta")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "idpauta", nullable = false)
    private Long idpauta;

    @NotBlank(message = "O titulo da pauta não pode ser branco.")
    @NotNull(message = "O titulo da pauta não pode ser nulo.")
    @ApiModelProperty(value = "Titulo da pauta.")
    @Size(min = 10, max = 1000, message = "Titulo da pauta tem que ter entre 10 e 1000 Caracteres.")
    @Column(name = "tituloPauta", nullable = false)
    private String tituloPauta;

    @NotBlank(message = "O nome do autor não pode ser branco.")
    @NotNull(message = "O nome do autor não pode ser nulo.")
    @ApiModelProperty(value = "ti da pauta.")
    @Size(min = 2, max = 1000, message = "Nome do Autor tem que ter entre 2 e 1000 Caracteres.")
    @Column(name = "autor", nullable = false)
    private String autor;

    @NotBlank(message = "O detalhamento da pauta não pode ser branco.")
    @NotNull(message = "O detalhamento da pauta não pode ser nulo.")
    @ApiModelProperty(value = "Detalhe da pauta.")
    @Size(min = 10, max = 1000, message = "Detalhamento da pauta tem que ter entre 10 e 1000 Caracteres.")
    @Column(name = "detalhePauta", nullable = false)
    private String detalhePauta;

    @NotBlank(message = "O status da pauta não pode ser branco.")
    @NotNull(message = "O status da pauta não pode ser nulo.")
    @ApiModelProperty(value = "Status da pauta : Criado, Encerrado, Ativo")
    @Column(name = "status", nullable = false)
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @NotBlank(message = "Data Criação da pauta não pode ser em branco")
    @NotNull(message = "Data Criação da pauta não pode ser em nulo.")
    @ApiModelProperty(value = "Data da pauta")
    @Column(name = "dataCriacaoPauta", nullable = false)
    private Date dataCriacaoPauta;

    @ApiModelProperty(value = "Lista de votos")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Voto.class)
    @JoinColumn(name = "fk_voto_pauta")
    @JsonIgnore
    private List<Voto> voto;

    @ApiModelProperty(value = "Lista de Associados")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Associado.class)
    @JoinColumn(name = "fk_pauta_Associado")
    @JsonIgnore
    private List<Associado> associado;

    public Long getIdpauta() {
        return idpauta;
    }

    public void setIdpauta(Long idpauta) {
        this.idpauta = idpauta;
    }

    public String getTituloPauta() {
        return tituloPauta;
    }

    public void setTituloPauta(String tituloPauta) {
        this.tituloPauta = tituloPauta;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getDetalhePauta() {
        return detalhePauta;
    }

    public void setDetalhePauta(String detalhePauta) {
        this.detalhePauta = detalhePauta;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDataCriacaoPauta() {
        return dataCriacaoPauta;
    }

    public void setDataCriacaoPauta(Date dataCriacaoPauta) {
        this.dataCriacaoPauta = dataCriacaoPauta;
    }

    public List<Voto> getVoto() {
        return voto;
    }

    public void setVoto(List<Voto> voto) {
        this.voto = voto;
    }

    public List<Associado> getAssociado() {
        return associado;
    }

    public void setAssociado(List<Associado> associado) {
        this.associado = associado;
    }
}





