package com.desafio.Pauta.PautaApi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "tbn_voto")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode()
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Voto {

    @Id
    @ApiModelProperty(value = "Id do voto")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "idVoto", nullable = false)
    private Long idVoto;

    @NotBlank(message = "O voto  da pauta não pode ser branco.")
    @NotNull(message = "O voto da pauta não pode ser nulo.")
    @ApiModelProperty(value = "voto do associado")
    @Size(min = 10, max = 15)
    @Column(name = "voto", nullable = false)
    private String voto;

    @NotBlank(message = "O data do voto não pode ser branco.")
    @NotNull(message = "O data da pauta não pode ser nulo.")
    @ApiModelProperty(value = "Data do voto")
    @Size(min = 10, max = 20)
    @Column(name = "Data_Registro", nullable = false)
    private Date data;

    @ApiModelProperty(value = "Lista de votos")
    @ManyToOne(targetEntity = Pauta.class)
    @JoinColumn(name = "fk_voto_pauta")
    private Pauta pauta;

    @ApiModelProperty(value = "Lista de votos")
    @ManyToOne(targetEntity = Associado.class)
    @JoinColumn(name = "fk_voto_Associado")
    private Associado associado;

    public Long getIdVoto() {
        return idVoto;
    }

    public void setIdVoto(Long idVoto) {
        this.idVoto = idVoto;
    }

    public String getVoto() {
        return voto;
    }

    public void setVoto(String voto) {
        this.voto = voto;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Pauta getPauta() {
        return pauta;
    }

    public void setPauta(Pauta pauta) {
        this.pauta = pauta;
    }

    public Associado getAssociado() {
        return associado;
    }

    public void setAssociado(Associado associado) {
        this.associado = associado;
    }
}
