package com.desafio.Pauta.PautaApi.controller;

import com.desafio.Pauta.PautaApi.dto.AssociadoDto;
import com.desafio.Pauta.PautaApi.dto.AssociadoRetornoDto;
import com.desafio.Pauta.PautaApi.service.AssociadoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

@RestController
@Api("Associado")
@RequestMapping("/api/Associado")
public class AssociadoController {

    private AssociadoService associadoService;

    @Autowired
    public AssociadoController(AssociadoService associadoService) {
        this.associadoService = associadoService;
    }

    @PostMapping("")
    @ApiOperation(httpMethod = "POST", value = "Cadastrar o Associado")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Associado cadastrado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao cadastrar Associado")})
    public ResponseEntity<AssociadoRetornoDto> cadastrarAssociado(@RequestBody @Valid AssociadoDto associadoDto) throws ParseException {
        AssociadoRetornoDto associadoRetornoDto = associadoService.cadastrarAssociado(associadoDto);
        return new ResponseEntity(associadoRetornoDto, HttpStatus.CREATED);
    }

    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "GET", value = "Listar todos os Associados Cadastrado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista o Associado"),
            @ApiResponse(code = 204, message = "Nenhum Associado encontrado!"),
            @ApiResponse(code = 400, message = "Erro!")})
    public ResponseEntity<List<AssociadoRetornoDto>> buscarAssociado() {
        List<AssociadoRetornoDto> associadoListDto = associadoService.buscarAssociado();
        return new ResponseEntity<List<AssociadoRetornoDto>>(associadoListDto, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "GET", value = "Listar Associado Cadastrado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista o Associado"),
            @ApiResponse(code = 204, message = "Nenhum Associado encontrado!"),
            @ApiResponse(code = 400, message = "Erro!")})
    public ResponseEntity<AssociadoRetornoDto> buscarAssociadoPorId(@PathVariable(value = "id") Long id) {
        AssociadoRetornoDto associadoRetornoDto = associadoService.buscarAssociadoPorId(id);
        return new ResponseEntity<AssociadoRetornoDto>(associadoRetornoDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "DELETE", value = "Deleta associado na base de dados.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "associado deletado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao deletar associado")})
    public ResponseEntity<String> detetarAssociado(@PathVariable(value = "id") Long id) {
        String result = associadoService.detetarAssociado(id);
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @PutMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "PUT", value = "Atualiza Associado na base de dados.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Associado atualizado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao atualizar Associado")})
    public ResponseEntity<AssociadoRetornoDto> atualizarAssociado(@RequestBody @Valid AssociadoDto associadoDto) {
        AssociadoRetornoDto associadoRetornoDto = associadoService.atualizarAssociado(associadoDto);
        return new ResponseEntity<AssociadoRetornoDto>(associadoRetornoDto, HttpStatus.CREATED);
    }

}
