package com.desafio.Pauta.PautaApi.controller;

import com.desafio.Pauta.PautaApi.dto.VotoAssociadoDto;
import com.desafio.Pauta.PautaApi.service.VotoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;

@RestController
@Api("Voto")
@RequestMapping("/api/voto")
public class VotoController {

    private VotoService votoService;

    @Autowired
    public VotoController(VotoService votoService) {
        this.votoService = votoService;
    }

    @PostMapping("/votar")
    @ApiOperation(httpMethod = "POST", value = "Cadastra o voto so Associado")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "voto cadastrado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao cadastrar voto")})
    public ResponseEntity<String> cadastrarVoto(@RequestBody @Valid VotoAssociadoDto votoAssociadoDto) throws ParseException {
        String voto = votoService.cadastrarVoto(votoAssociadoDto);
        return new ResponseEntity(voto, HttpStatus.CREATED);
    }

    @ExceptionHandler()
    @GetMapping(value = "/abrirSessao")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "GET", value = "Abre a sesssão do voto")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Sessão Aberta"),
            @ApiResponse(code = 204, message = "Erro ao abrir sessão"),
            @ApiResponse(code = 400, message = "Erro!")})
    public ResponseEntity<String> abrirSessaoVotacao(@RequestParam Long id, Long tempoEmMinuto) throws InterruptedException, ParseException {
        String sessao = votoService.abrirSessaoVotacao(id, tempoEmMinuto);
        ResponseEntity<String> stringResponseEntity = ResultadoVotacao(id);
        String body = stringResponseEntity.getBody();
        sessao = sessao + body;
        return new ResponseEntity<String>(sessao, HttpStatus.OK);
    }

    @GetMapping(value = "/Resultado/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "GET", value = "Listar o resultado final da Sessão")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Resultado Final da sessão"),
            @ApiResponse(code = 204, message = "Nenhuma resultado encontrado!"),
            @ApiResponse(code = 400, message = "Erro!")})
    public ResponseEntity<String> ResultadoVotacao(@PathVariable(value = "id") Long id) {
        String resultadoVotacao = votoService.resultadoVotacao(id);

        return new ResponseEntity<String >(resultadoVotacao, HttpStatus.OK);
    }

}
