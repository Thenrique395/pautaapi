package com.desafio.Pauta.PautaApi.controller;

import com.desafio.Pauta.PautaApi.dto.PautaDto;
import com.desafio.Pauta.PautaApi.dto.PautaRetornoDto;
import com.desafio.Pauta.PautaApi.service.PautaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;

@RestController
@Api("Pauta")
@RequestMapping("/api/Pauta")
public class PautaController {

    private PautaService servicePauta;

    @Autowired
    public PautaController(PautaService servicePauta) {
        this.servicePauta = servicePauta;
    }

    @PostMapping("")
    @ApiOperation(httpMethod = "POST", value = "Cadastrar a Pauta")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Pauta cadastrado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao cadastrar Pauta")})
    public ResponseEntity<PautaRetornoDto> cadastrarPauta(@RequestBody @Valid PautaDto pautaDto) throws ParseException {
        PautaRetornoDto pautaRetornoDto = servicePauta.cadastrarPauta(pautaDto);

        return new ResponseEntity(pautaRetornoDto, HttpStatus.CREATED);
    }

    @GetMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "GET", value = "Listar todas as Pauta")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista a Pauta"),
            @ApiResponse(code = 204, message = "Nenhuma Pauta encontrado!"),
            @ApiResponse(code = 400, message = "Erro!")})
    public ResponseEntity<List<PautaRetornoDto>> buscarPauta() {
        List<PautaRetornoDto> pautaRetornoDtos = servicePauta.buscarPauta();
        return new ResponseEntity<List<PautaRetornoDto>>(pautaRetornoDtos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "GET", value = "Listar Pauta Cadastrado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista a Pauta"),
            @ApiResponse(code = 204, message = "Nenhuma Pauta encontrado!"),
            @ApiResponse(code = 400, message = "Erro!")})
    public ResponseEntity<PautaRetornoDto> buscarPautaPorId(@PathVariable(value = "id") Long id) {
        PautaRetornoDto pautaRetornoDto = servicePauta.buscarPautaPorId(id);
        return new ResponseEntity<PautaRetornoDto>(pautaRetornoDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "DELETE", value = "Deleta Pauta na base de dados.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Pauta deletada com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao deletar Pauta")})
    public ResponseEntity<String> remover(@PathVariable(value = "id") Long id) throws ParseException {
        servicePauta.deletarPauta(id);
        return new ResponseEntity("Pauta excluida com sucesso.", HttpStatus.OK);
    }

    @PutMapping("")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "PUT", value = "Atualiza Pauta na base de dados.")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Pauta atualizada com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao atualizar Pauta")})
    public ResponseEntity<PautaRetornoDto> atualizar(@RequestBody @Valid PautaDto pautaDto) {
        PautaRetornoDto pautaRetornoDto = servicePauta.atualizarPauta(pautaDto);

        return new ResponseEntity(pautaRetornoDto, HttpStatus.OK);
    }

}
