package com.desafio.Pauta.PautaApi.service;

import com.desafio.Pauta.PautaApi.domain.Associado;
import com.desafio.Pauta.PautaApi.domain.Pauta;
import com.desafio.Pauta.PautaApi.dto.AssociadoDto;
import com.desafio.Pauta.PautaApi.dto.AssociadoRetornoDto;
import com.desafio.Pauta.PautaApi.dto.PautaRetornoDto;
import com.desafio.Pauta.PautaApi.exceptions.AssociadoException;
import com.desafio.Pauta.PautaApi.repository.AssociadoRepository;
import com.desafio.Pauta.PautaApi.util.ConvertAssociadoRetorno;
import com.desafio.Pauta.PautaApi.util.ConvertPautaRetornoDto;
import com.desafio.Pauta.PautaApi.util.CovertAssociado;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class AssociadoService {

    private AssociadoRepository associadoRepository;
    private ConvertAssociadoRetorno convertAssociadoRetorno;
    private CovertAssociado convertAssociado;
    private PautaService pautaService;
    private ConvertPautaRetornoDto convertPautaRetornoDto;
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(PautaService.class);


    @Autowired
    public AssociadoService(AssociadoRepository associadoRepository, ConvertAssociadoRetorno convertAssociadoRetorno, CovertAssociado convertAssociado, PautaService pautaService, ConvertPautaRetornoDto convertPautaRetornoDto) {
        this.associadoRepository = associadoRepository;
        this.convertAssociadoRetorno = convertAssociadoRetorno;
        this.convertAssociado = convertAssociado;
        this.pautaService = pautaService;
        this.convertPautaRetornoDto = convertPautaRetornoDto;
    }


    public AssociadoRetornoDto cadastrarAssociado(AssociadoDto associadodto) throws ParseException {
        PautaRetornoDto pautaRetornoDto = pautaService.buscarPautaPorId(associadodto.getFk_pauta_Associado());
        Pauta pauta = convertPautaRetornoDto.convertToEntity(pautaRetornoDto);
        pauta.setDataCriacaoPauta(new Date());
        if (pauta.getIdpauta() != null) {
            if (verificaCpf(associadodto.getCpf())) {
                Associado associado = convertAssociado.convertToEntity(associadodto);
                associado.setPauta(pauta);
                associadoRepository.save(associado);
                return convertAssociadoRetorno.convertToDTO(associado);
            } else {
                log.warn("Associado já cadastrado.");
                throw new AssociadoException("Associado já cadastrado.");
            }
        } else {
            log.warn("Pauta não cadastrada.");
            throw new AssociadoException("Pauta não cadastrada.");
        }
    }

    private AssociadoDto getByCpf(String cpf) {
        Associado associados = associadoRepository.findAllById(cpf);
        if (associados == null) {
            return new AssociadoDto();
        }
        return convertAssociado.convertToDTO(associados);
    }

    private boolean verificaCpf(String cpf) {
        Associado associados = associadoRepository.findAllById(cpf);
        if (associados == null) {
            return true;
        }
        log.warn("Cpf: " + cpf + " já existente");
        throw new AssociadoException("Cpf: " + cpf + " já existente");
    }

    public List<AssociadoRetornoDto> buscarAssociado() {
        List<Associado> associados = associadoRepository.findAll();
        return convertAssociadoRetorno.convertToDTO(associados);
    }

    public AssociadoRetornoDto buscarAssociadoPorId(Long id) {
        if (associadoRepository.existsById(id)) {
            Associado associado = associadoRepository.findById(id).get();
            AssociadoRetornoDto associadoRetornoDto = convertAssociadoRetorno.convertToDTO(associado);
            return associadoRetornoDto;
        } else {
            log.warn("Não foi encontrado o associado. Verificar se  associado esta cadastrada.");
            throw new AssociadoException("Não foi encontrado o associado. Verificar se  associado esta cadastrada.");
        }
    }

    public AssociadoRetornoDto buscarAssociadoPorCpf(String cpf) {
        Associado associado = associadoRepository.findAllById(cpf);
        AssociadoRetornoDto associadoRetornoDto = convertAssociadoRetorno.convertToDTO(associado);
        return associadoRetornoDto;

    }

    public String detetarAssociado(Long id) {
        if (associadoRepository.existsById(id)) {
            Associado associado = associadoRepository.removerAssociado(id);
            return " Associado  excluido com sucesso";
        } else
            log.warn("Associado não encontrado");
        throw new AssociadoException("Associado não encontrado");
    }

    public AssociadoRetornoDto atualizarAssociado(AssociadoDto associadoDto) {
        Associado associado1 = convertAssociado.convertToEntity(associadoDto);

        if (associadoRepository.existsById(associadoDto.getIdAssociado())) {
            AssociadoRetornoDto associadoRetornoDto = buscarAssociadoPorId(associadoDto.getIdAssociado());
            Associado associado = convertAssociadoRetorno.convertToEntity(associadoRetornoDto);

            associado.setCpf(associado1.getCpf());
            associado.setEmail(associado1.getEmail());
            associado.setNome(associado1.getNome());
            associado.getPauta().setIdpauta(associadoDto.getFk_pauta_Associado());

            Associado associadoAtualizado = associadoRepository.saveAndFlush(associado);
            return convertAssociadoRetorno.convertToDTO(associadoAtualizado);
        } else {
            log.warn("Erro ao atualizar associado. ");
            throw new AssociadoException("Erro ao atualizar associado. ");
        }
    }

}
