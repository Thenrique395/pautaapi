package com.desafio.Pauta.PautaApi.service;


import com.desafio.Pauta.PautaApi.domain.Pauta;
import com.desafio.Pauta.PautaApi.domain.constantes.Constantes;
import com.desafio.Pauta.PautaApi.dto.PautaDto;
import com.desafio.Pauta.PautaApi.dto.PautaRetornoDto;
import com.desafio.Pauta.PautaApi.exceptions.AssociadoException;
import com.desafio.Pauta.PautaApi.repository.PautaRepository;
import com.desafio.Pauta.PautaApi.util.ConvertPauta;
import com.desafio.Pauta.PautaApi.util.ConvertPautaRetornoDto;
import com.desafio.Pauta.PautaApi.util.ValidatorDate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;


@Slf4j
@Service
public class PautaService {

    private PautaRepository pautaRepository;
    private ConvertPauta convertPauta;
    private ConvertPautaRetornoDto convertPautaRetornoDtoPauta;
    private ValidatorDate validatorDate;

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(PautaService.class);

    @Autowired
    public PautaService(PautaRepository pautaRepository, ConvertPauta ConvertPauta, ConvertPautaRetornoDto convertPautaRetornoDtoPauta, ValidatorDate validatorDate) {
        this.pautaRepository = pautaRepository;
        this.convertPauta = ConvertPauta;
        this.convertPautaRetornoDtoPauta = convertPautaRetornoDtoPauta;
        this.validatorDate = validatorDate;
    }

    public PautaRetornoDto cadastrarPauta(PautaDto pautaDto) throws ParseException {

        Pauta pauta = convertPauta.convertToEntity(pautaDto);
        pauta.setStatus(Constantes.CRIADO);
        pauta.setDataCriacaoPauta(new Date(validatorDate.getDateTimeToString()));
        Pauta save = pautaRepository.save(pauta);
        PautaRetornoDto pautaRetornoDto = convertPautaRetornoDtoPauta.convertToDTO(save);

        return pautaRetornoDto;
    }


    public List<PautaRetornoDto> buscarPauta() {
        List<Pauta> pautas = pautaRepository.findAll();
        List<PautaRetornoDto> pautaDtoList = convertPautaRetornoDtoPauta.convertToDTO(pautas);
        return pautaDtoList;
    }

    public PautaRetornoDto buscarPautaPorId(Long id) {
        if (pautaRepository.existsById(id)) {
            Pauta pauta = pautaRepository.findById(id).get();
            PautaRetornoDto pautaRetornoDto = convertPautaRetornoDtoPauta.convertToDTO(pauta);
            return pautaRetornoDto;
        } else {
            log.warn("Não foi encontrado a pauta. Verificar se a pauta esta cadastrada.");
            throw new AssociadoException("Não foi encontrado a pauta. Verificar se a pauta esta cadastrada.");
        }
    }


    public void deletarPauta(Long id) {
        if (pautaRepository.existsById(id)) {
            pautaRepository.deleteById(id);
        } else {
            log.warn("Não foi possivel excluir a pauta. Verificar se a pauta esta cadastrada. :" + id + "");

            throw new AssociadoException("Não foi possivel excluir a pauta. Verificar se a pauta esta cadastrada. :" + id + "");
        }
    }

    public PautaRetornoDto atualizarPauta(Pauta pauta) {
        Pauta pautaAtualizado = pautaRepository.saveAndFlush(pauta);
        PautaRetornoDto pautaRetornoDto = convertPautaRetornoDtoPauta.convertToDTO(pautaAtualizado);
        return pautaRetornoDto;

    }

    public PautaRetornoDto atualizarPauta(PautaDto pautaDto) {
        if (pautaRepository.existsById(pautaDto.getIdpauta())) {
            Pauta pauta = convertPauta.convertToEntity(pautaDto);
            PautaRetornoDto pautaRetornoDto1 = buscarPautaPorId(pauta.getIdpauta());
            pauta.setDataCriacaoPauta(new Date());
            pauta.setStatus(Constantes.CRIADO);


            Pauta pautaAtualizado = pautaRepository.saveAndFlush(pauta);
            PautaRetornoDto pautaRetornoDto = convertPautaRetornoDtoPauta.convertToDTO(pautaAtualizado);
            return pautaRetornoDto;
        } else {
            log.warn("Não foi possivel atualizar a pauta. Verificar se a pauta esta cadastrada ou verifique os dados atualizados.");
            throw new AssociadoException("Não foi possivel atualizar a pauta. Verificar se a pauta esta cadastrada ou verifique os dados atualizados.");
        }

    }


}
