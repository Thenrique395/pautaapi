package com.desafio.Pauta.PautaApi.service;

import com.desafio.Pauta.PautaApi.ClientAPI.ConsultaCpfClient;
import com.desafio.Pauta.PautaApi.domain.Associado;
import com.desafio.Pauta.PautaApi.domain.Pauta;
import com.desafio.Pauta.PautaApi.domain.Voto;
import com.desafio.Pauta.PautaApi.domain.constantes.Constantes;
import com.desafio.Pauta.PautaApi.dto.AssociadoRetornoDto;
import com.desafio.Pauta.PautaApi.dto.PautaRetornoDto;
import com.desafio.Pauta.PautaApi.dto.VotoAssociadoDto;
import com.desafio.Pauta.PautaApi.exceptions.VotoException;
import com.desafio.Pauta.PautaApi.repository.VotoRepository;
import com.desafio.Pauta.PautaApi.util.*;
import com.sun.istack.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class VotoService {

    private VotoRepository votoRepository;
    private ConsultaCpfClient consultaCpfClient;
    private ConvertVoto convertVoto;
    private ConvertAssociadoRetorno convertAssociadoRetorno;
    private ConvertPautaRetornoDto convertPautaRetornoDto;
    private AssociadoService associadoService;
    private PautaService pautaService;
    private ValidatorDate validatorDate;
    private ScheduleTime scheduleTime;
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(PautaService.class);

    @Autowired
    public VotoService(VotoRepository votoRepository, ConsultaCpfClient consultaCpfClient, ConvertVoto convertVoto, ConvertAssociadoRetorno convertAssociadoRetorno, ConvertPautaRetornoDto convertPautaRetornoDto,
                       AssociadoService associadoService, PautaService pautaService, ValidatorDate validatorDate, ScheduleTime scheduleTime) {

        this.votoRepository = votoRepository;
        this.consultaCpfClient = consultaCpfClient;
        this.convertVoto = convertVoto;
        this.convertAssociadoRetorno = convertAssociadoRetorno;
        this.convertPautaRetornoDto = convertPautaRetornoDto;
        this.associadoService = associadoService;
        this.pautaService = pautaService;
        this.validatorDate = validatorDate;
        this.scheduleTime = scheduleTime;

    }

    public String cadastrarVoto(@NotNull VotoAssociadoDto votoAssociadoDto) throws ParseException {

        PautaRetornoDto pautaRetornoDto = pautaService.buscarPautaPorId(votoAssociadoDto.getPauta());
        Pauta pauta = convertPautaRetornoDto.convertToEntity(pautaRetornoDto);
        pauta.setDataCriacaoPauta(new Date());

        AssociadoRetornoDto associadoRetornoDto = associadoService.buscarAssociadoPorCpf(votoAssociadoDto.getCpfAssociado());
        Associado associado = convertAssociadoRetorno.convertToEntity(associadoRetornoDto);

        if (validaCpf(votoAssociadoDto.getCpfAssociado())) {
            if (ValidaSessao(votoAssociadoDto.getPauta().toString())) {
                if (votoRepository.validarVotoExistente(pauta.getIdpauta(), associado.getIdAssociado()) == null) {
                    Voto voto = convertVoto.convertToEntity(votoAssociadoDto);
                    voto.setData(validatorDate.getDateTime());
                    voto.setAssociado(associado);
                    voto.setPauta(pauta);
                    votoRepository.save(voto);
                } else {
                    log.warn("Associado já voto nessa pauta");
                    throw new VotoException("Associado já voto nessa pauta");
                }
            }
        }
        return "Voto Efetuado com sucesso. Obrigado!";
    }

    private boolean ValidaSessao(String id) {
        PautaRetornoDto pautaRetornoDto = pautaService.buscarPautaPorId(Long.parseLong(id));
        if (pautaRetornoDto.getStatus().contains(Constantes.ATIVO)) {
            return true;
        } else if (pautaRetornoDto.getStatus().contains(Constantes.ENCERRADO)) {
            log.warn("Sessão de votação Encerrado.");
            throw new VotoException("Sessão de votação Encerrado.");
        } else if (pautaRetornoDto.getStatus().contains(Constantes.CRIADO)) {
            log.warn("Sessão não iniciada.");
            throw new VotoException("Sessão não iniciada.");
        }
        return false;
    }

    private boolean validaCpf(String cpf) {
        String toVoto = consultaCpfClient.validarCpf(cpf);
        if (!toVoto.contains(Constantes.UNABLE_TO_VOTE)) {
            return true;
        } else {
            throw new VotoException("Cpf: " + cpf + " esta impossibilitado de votar.");
        }
    }

    public String abrirSessaoVotacao(Long id, Long tempo) throws InterruptedException, ParseException {
        PautaRetornoDto pautaRetornoDto = pautaService.buscarPautaPorId(id);
        Pauta pauta = convertPautaRetornoDto.convertToEntity(pautaRetornoDto);
        pauta.setStatus(Constantes.ATIVO);
        Date date = validatorDate.CovertToDate(pautaRetornoDto.getDataCriacaoPauta());
        pauta.setDataCriacaoPauta(date);
        pautaService.atualizarPauta(pauta);
        String retorno = scheduleTime.TimeSchedule(tempo);
        pauta.setStatus(Constantes.ENCERRADO);
        pautaService.atualizarPauta(pauta);

        return retorno;
    }

    public String resultadoVotacao(Long id) {
        Object sim = null;
        Object nao = null;
        List<Object[]> objects = votoRepository.resultadoVotacao(id);

        for (Object[] obj : objects) {
            sim = obj[0];
            nao = obj[1];
        }
        if (sim == null) {
            sim = 0;
        }
        if (nao == null) {
            nao = 0;
        }
        return " Resultado:  SIM: " + sim + "  NÃO: " + nao + "";
    }

}
