package com.desafio.Pauta.PautaApi.exceptions;

public class VotoException extends BaseException {
    public VotoException(String mensagem) {
        super(mensagem);
    }
}
