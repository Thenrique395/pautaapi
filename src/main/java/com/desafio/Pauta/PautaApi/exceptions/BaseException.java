package com.desafio.Pauta.PautaApi.exceptions;

public class BaseException extends RuntimeException {

    public BaseException(String mensagem) {
        super(mensagem, null, true, false);
    }
}
