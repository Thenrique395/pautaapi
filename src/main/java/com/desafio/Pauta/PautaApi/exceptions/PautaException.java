package com.desafio.Pauta.PautaApi.exceptions;

public class PautaException extends BaseException {
    public PautaException(String mensagem) {
        super("Sessão encontra-se fechada, não é possível realizar mais votações na sessão.");

    }

}
