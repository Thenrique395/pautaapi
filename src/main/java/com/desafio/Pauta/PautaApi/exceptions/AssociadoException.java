package com.desafio.Pauta.PautaApi.exceptions;

public class AssociadoException extends BaseException {

    public AssociadoException(String mensagem) {
        super(mensagem);
    }
}
