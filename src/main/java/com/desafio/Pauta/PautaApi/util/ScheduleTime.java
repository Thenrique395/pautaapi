package com.desafio.Pauta.PautaApi.util;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
//@EnableScheduling
public class ScheduleTime {
    private final int MilisegundosDefault = 60000;
    private final Long MinutosDefault = 1L;
    private int milisegundo;

    public String TimeSchedule(Long tempo) throws InterruptedException {

        if (tempo == null) {
            milisegundo = MilisegundosDefault;
            tempo = MinutosDefault;
        }
        int milisegundo = (int) ((tempo * 1000) * 60);

        System.out.println("Tempo inicial: " + LocalDateTime.now());
        System.out.println("Tempo Final: " + LocalDateTime.now().plusMinutes(tempo));
        Thread.sleep(milisegundo);
        System.out.println("Finalizado " + LocalDateTime.now());
        return "A sessão foi iniciada:" + LocalDateTime.now() + " e será finalizada: " + LocalDateTime.now().plusMinutes(tempo);
    }

}
