package com.desafio.Pauta.PautaApi.util;

import com.desafio.Pauta.PautaApi.domain.Pauta;
import com.desafio.Pauta.PautaApi.dto.PautaRetornoDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConvertPautaRetornoDto implements Converter<Pauta, PautaRetornoDto> {

    @Autowired
    ValidatorDate validatorDate;

    @Override
    public Pauta convertToEntity(PautaRetornoDto pautaDto) {
        Pauta entity = new Pauta();
        BeanUtils.copyProperties(pautaDto, entity);

        return entity;
    }

    @Override
    public PautaRetornoDto convertToDTO(Pauta entity) {
        PautaRetornoDto dto = new PautaRetornoDto();
        BeanUtils.copyProperties(entity, dto);
        dto.setDataCriacaoPauta(entity.getDataCriacaoPauta().toString());
        return dto;
    }

    @Override
    public List<PautaRetornoDto> convertToDTO(List<Pauta> entity) {

        PautaRetornoDto dto = new PautaRetornoDto();
        List<PautaRetornoDto> pautaRetornoDtos = new ArrayList<>();

        for (Pauta pauta : entity) {
            BeanUtils.copyProperties(pauta, dto);
            dto.setDataCriacaoPauta(pauta.getDataCriacaoPauta().toString());
            pautaRetornoDtos.add(dto);
            dto = new PautaRetornoDto();
        }
        return pautaRetornoDtos;
    }
}
