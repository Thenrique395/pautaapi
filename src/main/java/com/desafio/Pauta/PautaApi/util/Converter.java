package com.desafio.Pauta.PautaApi.util;

import java.util.List;

public interface Converter<T, Dto> {

    T convertToEntity(Dto dto);

    Dto convertToDTO(T entity);

    List<Dto> convertToDTO(List<T> entity);


}