package com.desafio.Pauta.PautaApi.util;

import com.desafio.Pauta.PautaApi.domain.Associado;
import com.desafio.Pauta.PautaApi.dto.AssociadoDto;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CovertAssociado implements Converter<Associado, AssociadoDto> {
    @Override
    public Associado convertToEntity(AssociadoDto associadoDto) {
        Associado entity = new Associado();
        BeanUtils.copyProperties(associadoDto, entity);
        return entity;
    }

    @Override
    public AssociadoDto convertToDTO(Associado entity) {
        AssociadoDto dto = new AssociadoDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    public List<AssociadoDto> convertToDTO(List<Associado> entity) {

        AssociadoDto dto = new AssociadoDto();
        List<AssociadoDto> associadoDtoList = new ArrayList<>();

        for (Associado associado : entity) {
            BeanUtils.copyProperties(associado, dto);
            associadoDtoList.add(dto);
            dto = new AssociadoDto();

        }
        return associadoDtoList;
    }
}
