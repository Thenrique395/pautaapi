package com.desafio.Pauta.PautaApi.util;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ValidatorDate {

    public Date CovertToDate(String date) throws ParseException {
//        String s = String.format(date,"yyyy-MM-dd HH:mm");
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date parseDate = format1.parse(date);
        return parseDate;

    }

    public String CovertDateToString(Date date) throws ParseException {
        return String.format(date.toString());
    }

    public Date getDateTime() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date();
        String format = dateFormat.format(date);
        return CovertToDate(format);
    }

    public String getDateTimeToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public boolean comparatorDate(Date after, Date before) {
        boolean data;
        if (after.before(before)) {
            data = true;
        } else if (after.after(before))
            data = false;
        else
            data = true;

        return data;
    }


}
