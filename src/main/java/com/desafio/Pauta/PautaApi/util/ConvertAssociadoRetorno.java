package com.desafio.Pauta.PautaApi.util;

import com.desafio.Pauta.PautaApi.domain.Associado;
import com.desafio.Pauta.PautaApi.dto.AssociadoRetornoDto;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConvertAssociadoRetorno implements Converter<Associado, AssociadoRetornoDto> {

    @Override
    public Associado convertToEntity(AssociadoRetornoDto associadoRetornoDto) {
        Associado entity = new Associado();
        BeanUtils.copyProperties(associadoRetornoDto, entity);
        return entity;
    }

    @Override
    public AssociadoRetornoDto convertToDTO(Associado entity) {
        AssociadoRetornoDto dto = new AssociadoRetornoDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    public List<AssociadoRetornoDto> convertToDTO(List<Associado> entity) {

        AssociadoRetornoDto dto = new AssociadoRetornoDto();
        List<AssociadoRetornoDto> associadoRetornoDtos = new ArrayList<>();

        for (Associado associado : entity) {
            BeanUtils.copyProperties(associado, dto);
            associadoRetornoDtos.add(dto);
            dto = new AssociadoRetornoDto();
        }
        return associadoRetornoDtos;
    }
}
