package com.desafio.Pauta.PautaApi.util;

import com.desafio.Pauta.PautaApi.domain.Voto;
import com.desafio.Pauta.PautaApi.dto.VotoAssociadoDto;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConvertVoto implements Converter<Voto, VotoAssociadoDto> {

    @Override
    public Voto convertToEntity(VotoAssociadoDto votoAssociadoDto) {
        Voto entity = new Voto();
        BeanUtils.copyProperties(votoAssociadoDto, entity);
        return entity;
    }

    @Override
    public VotoAssociadoDto convertToDTO(Voto entity) {
        VotoAssociadoDto dto = new VotoAssociadoDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    public List<VotoAssociadoDto> convertToDTO(List<Voto> entity) {

        VotoAssociadoDto dto = new VotoAssociadoDto();
        List<VotoAssociadoDto> votoAssociadoDtos = new ArrayList<>();

        for (Voto voto : entity) {
            BeanUtils.copyProperties(voto, dto);
            votoAssociadoDtos.add(dto);
            dto = new VotoAssociadoDto();
        }
        return votoAssociadoDtos;
    }
}
