package com.desafio.Pauta.PautaApi.util;

import com.desafio.Pauta.PautaApi.domain.Pauta;
import com.desafio.Pauta.PautaApi.dto.PautaDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ConvertPauta implements Converter<Pauta, PautaDto> {

    @Autowired
    ValidatorDate validatorDate;

    @Override
    public Pauta convertToEntity(PautaDto pautaDto) {
        Pauta entity = new Pauta();
        BeanUtils.copyProperties(pautaDto, entity);
        return entity;
    }

    @Override
    public PautaDto convertToDTO(Pauta entity) {
        PautaDto dto = new PautaDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    public List<PautaDto> convertToDTO(List<Pauta> entity) {

        PautaDto dto = new PautaDto();
        List<PautaDto> pautaDtoList = new ArrayList<>();

        for (Pauta pauta : entity) {
            BeanUtils.copyProperties(pauta, dto);
            pautaDtoList.add(dto);
            dto = new PautaDto();
        }
        return pautaDtoList;
    }
}