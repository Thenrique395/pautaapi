# Desafio Pauta - Sicredi

## Sobre o projeto

No cooperativismo, cada associado possui um voto e as decisões são tomadas em assembleias, por votação. Imagine que você deve criar uma solução backend para gerenciar essas sessões de votação.

Essa solução deve ser executada na nuvem e promover as seguintes funcionalidades através de uma API REST: 

●	Cadastrar uma nova pauta
●	Abrir uma sessão de votação em uma pauta (a sessão de votação deve ficar aberta por um tempo determinado na chamada de abertura ou 1 minuto por default)
●	Receber votos dos associados em pautas (os votos são apenas 'Sim'/'Não'. Cada associado é identificado por um id único e pode votar apenas uma vez por pauta)
●	Contabilizar os votos e dar o resultado da votação na pauta

Para fins de exercício, a segurança das interfaces pode ser abstraída e qualquer chamada para as interfaces pode ser considerada como autorizada. A escolha da linguagem, frameworks e bibliotecas é livre (desde que não infrinja direitos de uso).

É importante que as pautas e os votos sejam persistidos e que não sejam perdidos com o restart da aplicação.


### Regra de negocio:
  O banco esta sem dados. É necessario cadastrar uma pauta para depois cadastrar os associados.
  
   ### Heroku:
   A pautaAPI esta postado no heroku junto com o banco de dados postgres.
   
   ### Heroku  -  PostGres connection
   
    - Host: ec2-18-210-214-86.compute-1.amazonaws.com
    - Database: d3keh6a5jp75qe
    - User:   bpolbkocefpqso
    - Port: 5432
    - Password: abfafd5d2acf2b5abc0f2733df2a43ce4fef29547e46668892e0f2cfb810079a
    - URI: postgres://bpolbkocefpqso:abfafd5d2acf2b5abc0f2733df2a43ce4fef29547e46668892e0f2cfb810079a@ec2-18-210-214-86.compute-1.amazonaws.com:5432/d3keh6a5jp75qe
    - Heroku CLI:  heroku pg:psql postgresql-rectangular-94798 --app pautaapi
  
  ### Observação:
   Caso seja feito teste de carga e o aplicação nao tiver mais no ar. Será necessário fazer o deploy novamento
   
   - git push heroku master
  
  ### End Point Heroku:
https://pautaapi.herokuapp.com
  
  - Pauta    https://pautaapi.herokuapp.com/api/Pauta
  - Associado    https://pautaapi.herokuapp.com/api/Associado
  - Voto    https://pautaapi.herokuapp.com/api/Voto

### Tecnologias utilizadas

- Java 8
- SpringBoot
- Postgres Database
- Maven
Spring Cloud Feign client

### Tecnologias utilizadas para testes Automatizado e peformance.

- Jmeter -  Performance, carga
- Rest Assured -  Testes automatizado para testar API

### Ferramentas utilizadas

- Lombok 
- Swagger 
- Postman 
- Git 
- Jmeter
- GitLab
- IntelliJ IDEA



### Teste

Para testar a aplicação, seguir o seguinte caminho:

```
src > test > java > com.desafio.pautaAPI  (Escolher o arquivo que deseja executa o teste)
```

### Instruções para a execução

O projeto conta com uma classe "PautaApiApplication" dentro da API. Para startar a aplicação SpringBoot.

```
digitoUnico > src > main > java > com.desafio.pautaAPI > PautaApiApplication
```
via CMD

```bash
java -jar pautaAPI.jar
```

### Paths e endpoints

Path original da API:  [pautaAPI](https://pautaapi.herokuapp.com)

Endpoints: [Swagger UI]("https://pautaapi.herokuapp.com/swagger-ui.html")

### Contribuição

Alterações e novas melhorias fique a vontade para criar novos pull request.

### Contatos: 


Telefone : (81)99525-7823

Linkedin : [Tiago Henrique ]("https://www.linkedin.com/in/tiago-henrique-dos-santos-083979101/")

E-mail : "Thenrique395@gmail.com"
